import { check } from 'express-validator';

export const userSchemaValidation = () => {
  return [ 
    check('username', 'username does not Empty').not().isEmpty(),
    check('username', 'username must be Alphanumeric').isAlphanumeric(),
    check('username', 'username more than 6 degits').isLength({ min: 6 }),
    check('email', 'Invalid does not Empty').not().isEmpty(),
    check('email', 'Invalid email').isEmail()
  ]; 
}

export const userUpdateSchemaValidation = () => {
    return [ 
      check('username', 'username does not Empty').not().isEmpty(),
      check('username', 'username must be Alphanumeric').isAlphanumeric(),
      check('username', 'username more than 6 degits').isLength({ min: 6 }),
      check('email', 'Invalid does not Empty').not().isEmpty(),
      check('email', 'Invalid email').isEmail()
    ]; 
  }