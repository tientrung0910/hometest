/**
 * Required External Modules and Interfaces
 */

import express, { Request, Response } from "express";
import { BaseUser, Users, User} from "./users.interface";
import * as UserService from "./users.service";
import { userSchemaValidation, userUpdateSchemaValidation } from "./users.validation";
import { error } from "console";
var {validationResult} = require('express-validator');
/**
 * Router Definition
 */

export const usersRouter = express.Router();
/**
 * Controller Definitions
 */

// GET items

usersRouter.get("/", async (req: Request, res: Response) => {
    try {
      const users: Users[] = await UserService.findAll();
  
      res.status(200).send(users);
    } catch (e) {
      res.status(500).send(e.message);
    }
  });

  usersRouter.get("/:id", async (req: Request, res: Response) => {
    try {
      const users: Users[] = await UserService.findById(req.params.id);
  
      res.status(200).send(users);
    } catch (e) {
      res.status(500).send(e);
    }
  });

  usersRouter.post("/", userSchemaValidation(), async (req: Request, res: Response) => {
    try {
      console.log(req.body);
        const errors = validationResult(req);
        if (!errors.isEmpty()) {

          res.status(422).json({ errors: errors.array() });
          return;
        }

      const data: User = req.body; 
      const user: User = await UserService.create(data);
      res.status(200).send(user);
    } catch (e) {
      console.log(e);
        res.status(500).json({ errors: new Array({msg: e.message}) });
    }
  });

  usersRouter.patch("/:id", userUpdateSchemaValidation(), async (req: Request, res: Response) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          res.status(422).json({ errors: errors.array() });
          return;
        }

      const data: User = req.body; 
      const user: User = await UserService.update(req.params.id, data);
      res.status(200).send(user);
    } catch (e) {
        res.status(500).json({ errors: new Array({msg: e.message}) });
    }
  });

  usersRouter.delete("/:id", async (req: Request, res: Response) => {
    try {
      const user: User = await UserService.remove(req.params.id);
      res.status(200).send({msg: "user delete successful"});
    } catch (e) {
        console.log(e);
        res.status(500).json({ errors: new Array({msg: e.message}) });
    }
  });