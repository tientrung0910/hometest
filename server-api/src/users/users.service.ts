/**
 * Data Model Interfaces
 */

import { BaseUser, User, Users } from "./users.interface";
import { PrismaClient, Prisma } from "@prisma/client";

const prisma = new PrismaClient();
/**
 * Service Methods
 */

export const findAll = async (): Promise<Users> => {
 
    const users = await prisma.user.findMany();
    return users;
};

export const findById = async (id: number): Promise<User> => {
    const user = await prisma.user.findUnique({
        where: {
          id: parseInt(id)
        },
      });
      return user;
};

export const create = async (newItem: BaseUser): Promise<User> => {
    try{
    const user = await prisma.user.create({
        data: newItem
      });
      return user;
    }catch(e){      
        if (e instanceof Prisma.PrismaClientKnownRequestError) {
            // The .code property can be accessed in a type-safe manner
            if (e.code === 'P2002') {
              
               e.message = 'There is a unique constraint violation, a new user cannot be created with this email'
            }
          }
          throw e
    }
};

export const update = async (
  id: number,
  itemUpdate: BaseUser
): Promise<User | null> => {
    try{
    const updateUser = await prisma.user.update({
        where: {
          id: parseInt(id),
        },
        data: itemUpdate,
      });

      return updateUser;
    }catch(e){
        if (e instanceof Prisma.PrismaClientKnownRequestError) {
            // The .code property can be accessed in a type-safe manner
            if (e.code === 'P2002') {      
               e.message = 'There is a unique constraint violation, the user email cannot be update with this email'
            }
            if (e.code === 'P2025') {
                e.message = e.meta.cause
                }
          }
          throw e 
    }
};

export const remove = async (id: number): Promise<null | void> => {
    try{
        const deleteUser = await prisma.user.delete({
            where: {
                id: parseInt(id)
            }
        }) 
    }catch(e){
        if (e instanceof Prisma.PrismaClientKnownRequestError) {
            // The .code property can be accessed in a type-safe manner
            if (e.code === 'P2025') {
            e.message = e.meta.cause
            }
        }
        throw e     
    }
};