export interface BaseUser {
    email?: string;
    username: string | null;
    first_name: string | null;
    last_name: string | null;
    age: number | null;
  }

  export interface User extends BaseUser {
    id: number;
  }

  export interface Users {
    [key: number]: User;
  }