**1. install nodejs server api**
- cd server-api
- npm install
- npx prisma migrate dev
- npm run dev

**2. install python web server**
- python3 -m pip install requests
- cd server_web
- python3 manage.py runserver

**3. URL**
- nodejs API endpoint: http://localhost:1992/api/v1/
- python web server enpoint: 
   - admin enpoint: http://127.0.0.1:8000/admin
    - admin login: admin / admin
   
   - user fetch data enpoint: 
     - User list:   http://127.0.0.1:8000/user/show


**4. Result**

**- Successful tasks:**
 - API with Express and Node.js
    - Create a simple REST API using Express.
    - Implement CRUD operations for a "User" resource.
 - Web Server with Django and Python
    - Create a Django web server with one model, e.g., "Product".
    - Implement CRUD operations via Django's admin interface.
 - API-to-Database Connection
    - Connect your Express API to a SQLite database using any ORM.
    - Store and retrieve "User" data.
 - Integration
    - Make the Django web server fetch "User" data from the Express API.

**- Unsuccessful tasks:**
 - API Proxy with Python
   - Create a Python script that acts as a proxy.
   - Forward requests from the Python script to the Express API.
