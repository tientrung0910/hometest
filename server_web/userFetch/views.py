from django.shortcuts import render, redirect  
from product.forms import ProductForm  
from product.models import Product  
import requests
import json
# Create your views here.  
def create(request):  
    return render(request,"create.html")  
def post(request):  
    data = request.POST
    data = json.dumps(data)
    python_obj = json.loads(data)
    payload = { "username": python_obj.get("username"), "email": python_obj.get("email"), "first_name": python_obj.get("first_name"), "last_name": python_obj.get("last_name")}
    payload = json.dumps(payload)
    header = {"Content-Type": "application/json",
          "Accept": "text/plain"} 
    r = requests.post('http://localhost:1992/api/v1/user', data=payload, headers = header)
    if r.status_code == 200:
        res = json.loads(r.content)
        print(res)
    else:
        print(r.content)
        return render(request,"create.html", {"err":  json.loads(r.content)})  

    return redirect("/user/show")  
def show(request):  
    r = requests.get('http://localhost:1992/api/v1/user', params=request.GET)
    if r.status_code == 200:
        print(r.content)
        res = json.loads(r.content)
        return render(request,"show.html",{'users':res})  
def edit(request, id):  
    r = requests.get('http://localhost:1992/api/v1/user/' + str(id), params=request.GET)
    if r.status_code == 200:
        print(r.content)
        res = json.loads(r.content)
        return render(request,"edit.html",{'user':res})  
def update(request, id): 
     
    data = request.POST
    data = json.dumps(data)
    python_obj = json.loads(data)
    payload = { "username": python_obj.get("username"), "email": python_obj.get("email"), "first_name": python_obj.get("first_name"), "last_name": python_obj.get("last_name")}
    payload = json.dumps(payload)
    print(payload)
    header = {"Content-Type": "application/json",
          "Accept": "text/plain"} 
    r = requests.patch('http://localhost:1992/api/v1/user/' + str(id), data=payload, headers = header)
    r2 = requests.get('http://localhost:1992/api/v1/user/' + str(id), params=request.GET)
    res2 = json.loads(r2.content)
    if r.status_code == 200:
        res = json.loads(r.content)
        return render(request,"edit.html", {"success": "true"}) 
    else:
        print(r)
        return render(request,"edit.html", {"err": json.loads(r.content)}) 

def destroy(request, id):  
    r2 = requests.delete('http://localhost:1992/api/v1/user/' + str(id), params=request.GET)
    return redirect("/user/show") 