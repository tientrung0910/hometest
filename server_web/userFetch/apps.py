from django.apps import AppConfig


class UserfetchConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'userFetch'
