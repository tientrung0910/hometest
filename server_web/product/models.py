from django.db import models  
class Product(models.Model):  
    nid = models.CharField(max_length=20)  
    name = models.CharField(max_length=100)  
    description = models.CharField(max_length=255) 
    image = models.CharField(max_length=255)  
    class Meta:  
        db_table = "product"  